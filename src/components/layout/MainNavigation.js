import React from "react";
import { Link } from "react-router-dom";
import classes from "./MainNavigation.module.css";

const MainNavigation = function () {
  return (
    <header className={classes.header}>
      <div className={classes.logo}>React Meetups</div>
      <ul>
        <li>
          <Link to="/">All Meetups</Link>
        </li>
        <li>
          <Link to="/new-meetup">new meetup</Link>
        </li>
        <li>
          <Link to="/favorites">favorite</Link>
        </li>
      </ul>
    </header>
  );
};

export default MainNavigation;
