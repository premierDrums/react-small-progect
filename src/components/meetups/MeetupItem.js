import React from "react";
import Card from "../UL/Card";

import classes from "./MeetupItem.module.css";

const MeetupItem = function (props) {
  return (
    <div>
      <li className={classes.item}>
        <Card>
          <div className={classes.image}>
            <img src={props.image} alt={props.title} />
          </div>
          <div className={classes.content}>
            <h3>{props.title}</h3>
            <address>{props.address}</address>

            <p>{props.description}</p>
          </div>
          <div className={classes.actions}>
            <button>to favorite</button>
          </div>
        </Card>
      </li>
    </div>
  );
};

export default MeetupItem;
