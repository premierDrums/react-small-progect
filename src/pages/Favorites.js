import React from "react";
import { useContext } from "react";
import FavoritesContext from "../store/favorite-context";
import MeetupList from "../components/meetups/MeetupList";

const FavoritesPage = function () {
  const favoritesCtx = useContext(FavoritesContext);
  return (
    <div>
      <h1>My Favorites</h1>
      <MeetupList meetups={favoritesCtx.favorites} />
    </div>
  );
};

export default FavoritesPage;
